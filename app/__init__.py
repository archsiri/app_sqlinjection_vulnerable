from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api


app = Flask(__name__)
bootstrap = Bootstrap()
db = SQLAlchemy()
api = Api(app)

def create_app(config):
    app.config.from_object(config)
    bootstrap.init_app(app)

    from .models import User

    from .views import page as views_blueprint
    app.register_blueprint(views_blueprint)

    with app.app_context():
        db.init_app(app)
        db.create_all()

    return app