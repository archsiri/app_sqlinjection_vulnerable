from flask_table import Table, Col

class Results(Table):
    id = Col('id')
    username = Col('username')
    email = Col('email')
